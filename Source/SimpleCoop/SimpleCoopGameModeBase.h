// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimpleCoopGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLECOOP_API ASimpleCoopGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
