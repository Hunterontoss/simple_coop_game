#include "SCExplosiveBarrel.h"
#include "Component/SC_HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "PhysicsEngine/RadialForceComponent.h"

ASCExplosiveBarrel::ASCExplosiveBarrel()
{
	SetReplicates(true);
	SetReplicateMovement(true);

	HealthComponent = CreateDefaultSubobject<USC_HealthComponent>(TEXT("HealthComp"));
	HealthComponent->OnHealthChanged.AddDynamic(this, &ASCExplosiveBarrel::OnHealthChanged);
	HealthComponent->SetIsReplicated(true);

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetSimulatePhysics(true);
	MeshComponent->SetCollisionObjectType(ECC_PhysicsBody);
	SetRootComponent(MeshComponent);

	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	RadialForceComponent->SetupAttachment(MeshComponent);
	RadialForceComponent->Radius = 250.f;
	RadialForceComponent->bImpulseVelChange = true;
	RadialForceComponent->bAutoActivate = false;
	RadialForceComponent->bIgnoreOwningActor = true;

	ExplosionImpulse = 400.f;
}

void ASCExplosiveBarrel::OnHealthChanged(USC_HealthComponent* HealthComp, float Health, float DeltaHealth,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (bExploded) return;

	if (Health <= 0.f)
	{
		bExploded = true;
		OnRep_Exploded();

		const FVector BoostIntensity = FVector::UpVector * ExplosionImpulse;
		MeshComponent->AddImpulse(BoostIntensity, NAME_None, true);

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
		MeshComponent->SetMaterial(0, ExplodedMaterial);

		RadialForceComponent->FireImpulse();
	}
}

void ASCExplosiveBarrel::OnRep_Exploded()
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());

	MeshComponent->SetMaterial(0, ExplodedMaterial);
}

void ASCExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASCExplosiveBarrel, bExploded);
}