#include "SC_Character.h"
#include "SC_Weapon.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "SimpleCoop/SimpleCoop.h"
#include "Component/SC_HealthComponent.h"
#include "Net/UnrealNetwork.h"


ASC_Character::ASC_Character()
{
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<USC_HealthComponent>(TEXT("HealthComponent"));
	HealthComponent->SetIsReplicated(true);
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	GetMovementComponent()->NavAgentProps.bCanCrouch = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	ZoomedFOV = 65.f;
	ZoomInterSpeed = 20.f;
	WeaponAttachSocketName = "WeaponSocket";
}

void ASC_Character::BeginPlay()
{
	Super::BeginPlay();

	DefaultFOV = CameraComponent->FieldOfView;
	HealthComponent->OnHealthChanged.AddDynamic(this, &ASC_Character::OnHealthChanged);

	if (GetLocalRole() == ROLE_Authority)
	{
		SpawnWeapon();
	}
}

void ASC_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const float TargetFOV = bWantToZoom ? ZoomedFOV : DefaultFOV;

	const float NewFOV = FMath::FInterpTo(CameraComponent->FieldOfView, TargetFOV, DeltaTime, ZoomInterSpeed);

	CameraComponent->SetFieldOfView(NewFOV);
}

void ASC_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASC_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASC_Character::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ASC_Character::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ASC_Character::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASC_Character::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASC_Character::EndCrouch);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);

	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ASC_Character::BeginZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ASC_Character::EndZoom);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASC_Character::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASC_Character::StopFire);
}

void ASC_Character::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void ASC_Character::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void ASC_Character::BeginZoom()
{
	bWantToZoom = true;
}

void ASC_Character::EndZoom()
{
	bWantToZoom = false;
}

FVector ASC_Character::GetPawnViewLocation() const
{
	if (CameraComponent)
	{
		return CameraComponent->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

void ASC_Character::SpawnWeapon()
{
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		CurrentWeapon = GetWorld()->SpawnActor<ASC_Weapon>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParameters);

		if (CurrentWeapon)
		{
			CurrentWeapon->SetOwner(this);
			CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
		}
	}
}


void ASC_Character::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ASC_Character::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void ASC_Character::BeginCrouch()
{
	Crouch();
}

void ASC_Character::EndCrouch()
{
	UnCrouch();
}

void ASC_Character::OnHealthChanged(USC_HealthComponent* HealthComp, float Health, float DeltaHealth,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.f && !bDied)
	{
		bDied = true;
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.f);
	}
}

void ASC_Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASC_Character, CurrentWeapon);
	DOREPLIFETIME(ASC_Character, bDied);
}