// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_Weapon.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "SimpleCoop/SimpleCoop.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef
CVarDebugWeaponDrawing(
	TEXT("COOP.DebugWeapons"),
	DebugWeaponDrawing,
	TEXT("Draw debug lines for weapon"),
	ECVF_Cheat);


ASC_Weapon::ASC_Weapon()
{

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	MuzzleSocketName = "MuzzleSocket";
	TracerTargetPoint = "BeamEnd";
	BaseDamage = 20.f;
	FireRate = 600;

	SetReplicates(true);
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
}

void ASC_Weapon::BeginPlay()
{
	Super::BeginPlay();

	if (FireRate != 0)
	{
		TimeBetweenShot = 60 / FireRate;	
	}
}

void ASC_Weapon::StartFire()
{
	const float FirstDelay = FMath::Max(LastTimeFire + TimeBetweenShot - GetWorld()->TimeSeconds, 0.f);
	
	GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this, &ASC_Weapon::Fire, TimeBetweenShot, true, FirstDelay );
}

void ASC_Weapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

void ASC_Weapon::Fire()
{
	
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerFire();
	}
	
	const auto* MyOwner = GetOwner();

	if (MyOwner)
	{
		FVector EyeLocation;
		FRotator EyeRotation;

		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();
		FVector TraceEnd = EyeLocation + ShotDirection * 10000;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner);
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		FVector TracerEndPoint = TraceEnd;
		EPhysicalSurface SurfaceType = SurfaceType_Default;

		FHitResult HitResult;
		bool Result = GetWorld()->LineTraceSingleByChannel(HitResult, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams);
		
		if (Result)
		{
			auto* HitActor = HitResult.GetActor();

			SurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());
			float ActualDamage = BaseDamage;			
			if (SurfaceType == SURFACE_FLESHVULNERABLE)
			{
				ActualDamage*= 3.f;
			}
			
			UGameplayStatics::ApplyPointDamage(HitActor, ActualDamage, ShotDirection, HitResult, MyOwner->GetInstigatorController(), this, DamageType);

			PlayImpactEffect(SurfaceType, HitResult.ImpactPoint);

			TracerEndPoint = HitResult.ImpactPoint;
		}

		if (DebugWeaponDrawing > 1)
		{
			DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::Red, false, 1.f, 1.f);
		}

		PlayFireEffect(TracerEndPoint);

		if (GetLocalRole() == ROLE_Authority)
		{
			HitScanTrace.TraceTo = TracerEndPoint;
			HitScanTrace.SurfaceType = SurfaceType;
		}

		LastTimeFire = GetWorld()->TimeSeconds;
	}
}

void ASC_Weapon::ServerFire_Implementation()
{
	Fire();
}

bool ASC_Weapon::ServerFire_Validate()
{
	return true;
}

void ASC_Weapon::OnRep_HitScanTrace()
{
	// For Cosmetic
	PlayFireEffect(HitScanTrace.TraceTo);
	PlayImpactEffect(HitScanTrace.SurfaceType, HitScanTrace.TraceTo);

}

void ASC_Weapon::PlayFireEffect(FVector TraceEnd)
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComponent, MuzzleSocketName);
	}

	if (MuzzleEffect)
	{
		FVector MuzzleLocation = MeshComponent->GetSocketLocation(MuzzleSocketName);
		auto ParticleComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		if (ParticleComponent)
		{
			ParticleComponent->SetVectorParameter(TracerTargetPoint, TraceEnd);
		}
	}

	const auto* MyOwner = Cast<APawn>(GetOwner());

	if (MyOwner)
	{
		auto* PC = Cast<APlayerController>(MyOwner->GetController());

		if (PC)
		{
			PC->ClientPlayCameraShake(FireCameraShake);
		}
	}
}

void ASC_Weapon::PlayImpactEffect(const EPhysicalSurface SurfaceType, const FVector ImpactPoint) const
{
	UParticleSystem* SelectedEffect = nullptr;
	
	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		SelectedEffect = FleshImpactEffect;
		break;
	default:
		SelectedEffect = DefaultImpactEffect;
		break;
	}

	if (SelectedEffect)
	{
		const FVector MuzzleLocation = MeshComponent->GetSocketLocation(MuzzleSocketName);
		
		FVector ShootDirection = ImpactPoint - MuzzleLocation;
		ShootDirection.Normalize();

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, ImpactPoint, ShootDirection.Rotation());
	}
}

void ASC_Weapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASC_Weapon, HitScanTrace, COND_SkipOwner);
}
