#include "Component/SC_HealthComponent.h"

#include "Net/UnrealNetwork.h"

USC_HealthComponent::USC_HealthComponent()
{
	DefaultHealth = 100.f;
	Health = DefaultHealth;
}

void USC_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* MyOwner = GetOwner();

		if (MyOwner)
		{
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &USC_HealthComponent::HandleTakeAnyDamage);
		}
	}
}

void USC_HealthComponent::Heal(const float HealAmount)
{
	if (HealAmount <= 0.f || Health <= 0.f) return;

	Health = FMath::Clamp(Health + HealAmount, 0.f, DefaultHealth);

	UE_LOG(LogTemp, Log, TEXT("Health Change %s (+%s) "), *FString::SanitizeFloat(Health), *FString::SanitizeFloat(HealAmount));

	OnHealthChanged.Broadcast(this, Health, -HealAmount, nullptr, nullptr, nullptr);
}

void USC_HealthComponent::OnRep_Health(float OldHealth)
{
	const float Damage = Health - OldHealth;

	OnHealthChanged.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);
}

void USC_HealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage,
                                              const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.f) return;

	Health = FMath::Clamp(Health - Damage, 0.f, DefaultHealth);

	UE_LOG(LogTemp, Log, TEXT("Health Change %s"), *FString::SanitizeFloat(Health));

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);
}

void USC_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USC_HealthComponent, Health);
}

