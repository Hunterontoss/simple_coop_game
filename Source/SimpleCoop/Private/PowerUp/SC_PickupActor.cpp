#include "PowerUp/SC_PickupActor.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "PowerUp/SC_PowerUp.h"

ASC_PickupActor::ASC_PickupActor()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComponent->SetSphereRadius(75.f);
	SetRootComponent(SphereComponent);

	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComponent"));
	DecalComponent->SetRelativeRotation(FRotator(90.f, 0.f, 0.f));
	DecalComponent->DecalSize = FVector(64.f, 75.f, 75.f);
	DecalComponent->SetupAttachment(RootComponent);

	RespawnCooldown = 10.f;

	SetReplicates(true);
}

void ASC_PickupActor::BeginPlay()
{
	Super::BeginPlay();

	check(PowerUpClass);

	if (GetLocalRole() == ROLE_Authority)
	{
		Respawn();
	}
	
}

void ASC_PickupActor::Respawn()
{
	if (!PowerUpClass) return;
		
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	PowerUpInstance = GetWorld()->SpawnActor<ASC_PowerUp>(PowerUpClass, GetActorTransform(), SpawnParameters);

}


void ASC_PickupActor::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (GetLocalRole() == ROLE_Authority && PowerUpInstance)
	{
		PowerUpInstance->ActivatePowerUp(OtherActor);
		PowerUpInstance = nullptr;

		GetWorldTimerManager().SetTimer(TimerHandleRespawn, this, &ASC_PickupActor::Respawn, RespawnCooldown);
	}
}

