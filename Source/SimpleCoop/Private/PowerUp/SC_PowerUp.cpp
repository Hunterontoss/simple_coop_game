#include "PowerUp/SC_PowerUp.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASC_PowerUp::ASC_PowerUp()
{

	PowerUpInterval = 0.f;
	TotalNumberOfTick = 0.f;

	bIsPowerUpActive = false;

	SetReplicates(true);
}

void ASC_PowerUp::ActivatePowerUp(AActor* ActiveFor)
{
	OnActivate(ActiveFor);

	bIsPowerUpActive = true;
	OnRep_PowerUpActive();

	if (PowerUpInterval > 0)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_PowerUpTick, this, &ASC_PowerUp::OnTickPowerUp, PowerUpInterval, true);
	}
	else
	{
		OnTickPowerUp();
	}
}

void ASC_PowerUp::OnTickPowerUp()
{
	TicksProcessed++;

	OnPowerUpTick();

	if (TicksProcessed >= TotalNumberOfTick)
	{
		OnExpired();

		bIsPowerUpActive = false;
		OnRep_PowerUpActive();

		GetWorldTimerManager().ClearTimer(TimerHandle_PowerUpTick);
	}
}

void ASC_PowerUp::OnRep_PowerUpActive()
{
	OnPowerUpStateChanged(bIsPowerUpActive);
}

void ASC_PowerUp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASC_PowerUp, bIsPowerUpActive);
	
}
