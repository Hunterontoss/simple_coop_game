#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SC_Character.generated.h"

class UCameraComponent;
class USpringArmComponent;
class ASC_Weapon;
class USC_HealthComponent;

UCLASS()
class SIMPLECOOP_API ASC_Character : public ACharacter
{
	GENERATED_BODY()

public:
	ASC_Character();

protected:
	virtual void BeginPlay() override;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void BeginCrouch();
	void EndCrouch();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USpringArmComponent* SpringArmComponent;

	// === HEALTH ===
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USC_HealthComponent* HealthComponent;

	UFUNCTION()
	void OnHealthChanged(USC_HealthComponent* HealthComp, float Health, float DeltaHealth, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Player)
	bool bDied;
	// === ===
	
	bool bWantToZoom;
	float DefaultFOV;

	UPROPERTY(EditDefaultsOnly, Category = Player)
	float ZoomedFOV;

	UPROPERTY(EditDefaultsOnly, Category=Player, meta = (ClampMin=0.1, ClampMax=100))
	float ZoomInterSpeed;

	void BeginZoom();
	void EndZoom();

	UPROPERTY(Replicated)
	ASC_Weapon* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, Category = Player)
	TSubclassOf<ASC_Weapon> StarterWeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = Player)
	FName WeaponAttachSocketName;

	void StartFire();
	void StopFire();

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;
	
private:
	void SpawnWeapon();
};
