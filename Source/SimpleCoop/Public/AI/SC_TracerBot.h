// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SC_TracerBot.generated.h"

class USC_HealthComponent;
class USphereComponent;
class USoundCue;

UCLASS()
class SIMPLECOOP_API ASC_TracerBot : public APawn
{
	GENERATED_BODY()

public:
	ASC_TracerBot();

	void OnCheckNearbyBots();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = Components)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Components)
	USC_HealthComponent* HealthComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Components)
	USphereComponent* SphereComponent;

	FVector GetNextPathPoint();
	FVector NextPathPoint;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	float MovementForce;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	bool bUseVelocityChange;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	float RequiredDistanceToTarget;

	UFUNCTION()
	void HandleTakeDamage(USC_HealthComponent* HealthComp, float Health, float DeltaHealth,
		const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY()
	UMaterialInstanceDynamic* PulseMaterialInstance;
	
	void SelfDestruct();

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	UParticleSystem* ExplosionEffect;

	bool bExploded;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	float ExplosionDamage;
	
	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	float SelfDamageInterval;

	bool bStartedSelfDestruction;
	FTimerHandle TimerHandle_SelfDamage;
	void SelfDamage();

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	USoundCue* SelfDestructSound;

	UPROPERTY(EditDefaultsOnly, Category = TracerBot)
	USoundCue* ExplosionSound;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

private:

	int32 PowerLevel;
};
