// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SCExplosiveBarrel.generated.h"

class USC_HealthComponent;
class UStaticMeshComponent;
class URadialForceComponent;
class UParticleSystem;

UCLASS()
class SIMPLECOOP_API ASCExplosiveBarrel : public AActor
{
	GENERATED_BODY()
	
public:	
	ASCExplosiveBarrel();

protected:
	UPROPERTY(Replicated, VisibleAnywhere, Category=Components)
	USC_HealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, Category=Components)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, Category=Components)
	URadialForceComponent* RadialForceComponent;

	UFUNCTION()
	void OnHealthChanged(USC_HealthComponent* HealthComp, float Health, float DeltaHealth, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(ReplicatedUsing=OnRep_Exploded)
	bool bExploded;

	UFUNCTION()
	void OnRep_Exploded();

	UPROPERTY(EditDefaultsOnly, Category=FX)
	float ExplosionImpulse;

	UPROPERTY(EditDefaultsOnly, Category=FX)
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category=FX)
	UMaterialInterface* ExplodedMaterial;
};
