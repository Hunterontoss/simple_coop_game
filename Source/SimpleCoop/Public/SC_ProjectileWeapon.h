#pragma once

#include "CoreMinimal.h"
#include "SC_Weapon.h"
#include "SC_ProjectileWeapon.generated.h"

UCLASS()
class SIMPLECOOP_API ASC_ProjectileWeapon : public ASC_Weapon
{
	GENERATED_BODY()

protected:

	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly, Category = ProjectileWeapon)
	TSubclassOf<AActor> ProjectileClass;
};
