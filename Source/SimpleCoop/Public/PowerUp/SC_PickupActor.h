// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SC_PickupActor.generated.h"

class USphereComponent;
class UDecalComponent;
class ASC_PowerUp;

UCLASS()
class SIMPLECOOP_API ASC_PickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASC_PickupActor();

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = Components)
	USphereComponent* SphereComponent;

	UPROPERTY(VisibleAnywhere, Category = Components)
	UDecalComponent* DecalComponent;

	UPROPERTY(EditInstanceOnly, Category = PickUp)
	TSubclassOf<ASC_PowerUp> PowerUpClass;

	ASC_PowerUp* PowerUpInstance;

	UPROPERTY(EditInstanceOnly, Category = PickUp)
	float RespawnCooldown;

	FTimerHandle TimerHandleRespawn;

	void Respawn();
};
