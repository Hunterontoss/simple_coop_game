// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SC_PowerUp.generated.h"

UCLASS()
class SIMPLECOOP_API ASC_PowerUp : public AActor
{
	GENERATED_BODY()
	
public:	
	ASC_PowerUp();
	
	UFUNCTION(BlueprintImplementableEvent, Category = PowerUps)
	void OnActivate(AActor* ActiveFor);

	UFUNCTION(BlueprintImplementableEvent, Category = PowerUps)
	void OnPowerUpTick();

	UFUNCTION(BlueprintImplementableEvent, Category = PowerUps)
	void OnExpired();

	void ActivatePowerUp(AActor* ActiveFor);

protected:

	/* Time between power up ticks */
	UPROPERTY(EditDefaultsOnly, Category = PowerUps)
	float PowerUpInterval;

	/* Total times wea apply the power up effect */
	UPROPERTY(EditDefaultsOnly, Category = PowerUps)
	int32 TotalNumberOfTick;

	int32 TicksProcessed;

	FTimerHandle TimerHandle_PowerUpTick;

	UFUNCTION()
	void OnTickPowerUp();

	UPROPERTY(ReplicatedUsing = OnRep_PowerUpActive)
	bool bIsPowerUpActive;

	UFUNCTION()
	void OnRep_PowerUpActive();

	UFUNCTION(BlueprintImplementableEvent, Category = PowerUps)
	void OnPowerUpStateChanged(bool bNewIsActive);
};
