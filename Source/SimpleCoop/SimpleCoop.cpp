// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimpleCoop.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SimpleCoop, "SimpleCoop" );
